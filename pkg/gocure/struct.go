package gocure

import (
	"gitlab.com/kaj.fehlhaber/gocure/embedded"
	"gitlab.com/kaj.fehlhaber/gocure/report/html"
)

type HTML struct {
	Config html.Data
}

type Embedded struct {
	Config embedded.Data
}
